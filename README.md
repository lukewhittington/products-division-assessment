# Products Division Interview Assessment

## Background

The Pet Clinic is a client of the Cognitran Products Division. The Pet Clinic software stores information about vets, owners and pets. Visits to a surgery by an owner and a pet are also recorded by this software.

## Software

The Pet Clinic is a web application built using the Spring Boot framework. Data is stored in a relational database. The user interface is rendered from Thymeleaf templates that can be found in the `resources/templates` directory. The database is queried using Spring Data repositories via JPA.

##Task

Currently, the software tracks the following information about an owner:

- First Name
- Last Name
- Address
- Address City
- Telephone
- Pets

The client has asked for a change to be made to the software to also store an owner’s gender. This field should be optional. The user must be able to set an existing owner’s gender as well as being able to set a new owner’s gender. The owner list page should also display the gender in a new column on the existing table. We’d like you to spend the next 45 minutes tackling this task.

We actively encourage you to ask us any questions that you feel relevant throughout the entire exercise. You should also feel free to search the internet for any additional information you may need.

We encourage you to discuss your approach when accomplishing the task. We care more about your methodology than your success within the timeframe.
